FROM ubuntu 
ADD script2.sh /
COPY . .
RUN bash -c "chmod a+rwx script2.sh"
RUN bash -c "./script2.sh"
EXPOSE 80
#CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
